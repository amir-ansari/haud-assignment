package com.haud.api.impl;

import com.haud.entity.SimCard;

public interface SimCardService {
	long createSimCard(SimCard simCard,String userName);
}
