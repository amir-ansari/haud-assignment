
**Setup**

 - Import project as maven project using any ide
 - Project can be run by Running main class TelecomApplication.java
 - Project will start on port 9090

**Steps to run**
 - mvn clean install
 - java -jar TelecommunicationOperator-exec.jar
 
**Documentation**

 - http://localhost:9090/haud/swagger-ui.html
 
**Project Url**

 - base url: localhost:9090/haud
 - customer url: localhost:9090/haud/customer
 - sim card url: localhost:9090/haud/simcard
 
**Database**

-  Database console: http://localhost:9090/haud/h2-console

**Points taken care**

- Code quality
- Naming convention
- Test coverage